import re
from datetime import date
from datetime import datetime, timedelta

from django.core.management.base import BaseCommand

from dit_enterprisehub_sap.models import *


class Command(BaseCommand):
    help = "Create new transaction for porting request"

    def add_arguments(self, parser):
        parser.add_argument("person", type=int)
        parser.add_argument("custnr", type=int)

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        person = options["person"]
        custnr = options["custnr"]

        all_numbers = (
            DolTelCusNum.objects.using("sapserver")
            .exclude(u_wbciidin=None)
            .filter(u_carrier=2)
            .filter(u_custnr=custnr)
            .filter(u_portdate__gte=date.today())
            .order_by("u_custnr")
        )

        for telnum in all_numbers:
            self.stdout.write(
                self.style.SUCCESS(
                    telnum.u_custnr.cardname
                    + " Number: "
                    + str(telnum.u_onkz)
                    + "/"
                    + str(telnum.u_mainnum)
                    + " === "
                    + telnum.u_wbciidin
                )
            )

            m = re.search("^(.+)\s(\S+)$", telnum.u_custnr.address)

            self.stdout.write(telnum.u_custnr.address)
            self.stdout.write(m.group(0))
            self.stdout.write(m.group(1))
            self.stdout.write(m.group(2))

            datenow = date.today()
            timenow = datetime.now()
            port_date = telnum.u_portdate
            port_date_old = telnum.u_portdate

            paid_until = port_date_old - timedelta(days=1)

            onnm_entity = Onnm.objects.using("sapserver").get(objectcode="DOL_CTR")
            onnm_entity.refresh_from_db()  # make sure, AutoKey is Up2Date

            new_contract = DolCtrContracts(
                code=int("4500" + str(onnm_entity.autokey)),
                docentry=onnm_entity.autokey,
                canceled="N",
                object="DOL_CTR",
                usersign=4,
                transfered="N",
                createdate=datenow,
                createtime=timenow.strftime("%H%M"),
                updatedate=datenow,
                updatetime=timenow.strftime("%H%M"),
                datasource="I",
                u_contrtype=DolCtrContrTypes.objects.using("sapserver").get(pk=2),
                u_custnr=telnum.u_custnr,
                u_txtreference="",
                u_price=0,
                u_discountpercent=0,
                u_datestart=port_date_old,
                u_contrterms=DolCtrContrTimef.objects.using("sapserver").get(pk=3),
                u_paiduntil=paid_until,
                u_contrstate=DolCtrContrState.objects.using("sapserver").get(pk=1),
                u_cancelcycle=0,
                u_artnr=Oitm.objects.using("sapserver").get(pk="DTELNUMDELAN"),
            )

            onnm_entity.autokey += 1
            onnm_entity.save()

            new_contract.save(force_insert=True, using="sapserver")
            new_contract.refresh_from_db()

            onnm_entity2 = Onnm.objects.using("sapserver").get(
                objectcode="DOL_TEL_CE_TA"
            )
            onnm_entity2.refresh_from_db()  # make sure, AutoKey is Up2Date

            datecode = timenow.strftime("%Y%m%d")

            try:
                latestline = (
                    DolTelCeTransact.objects.using("sapserver")
                    .filter(code__contains=datecode)
                    .latest("code")
                )
                transactno = int(latestline.code) + 1
            except DolTelCeTransact.DoesNotExist:
                transactno = int(timenow.strftime("%Y%m%d") + "000001")

            if telnum.u_blockto is None:
                transacttype = DolTelCeTaActn.objects.using("sapserver").get(code=1)
                quantity = 1
            else:
                transacttype = DolTelCeTaActn.objects.using("sapserver").get(code=2)
                person = 0
                quantity = telnum.u_blockto - telnum.u_blockfrom + 1

            new_transact = DolTelCeTransact(
                code=transactno,
                docentry=onnm_entity2.autokey,
                canceled="N",
                object="DOL_TEL_CE_TA",
                usersign=4,
                transfered="N",
                createdate=datenow,
                createtime=timenow.strftime("%H%M"),
                updatedate=datenow,
                updatetime=timenow.strftime("%H%M"),
                datasource="I",
                u_partnerno=6676394,
                u_referencenopartner=new_contract,
                u_desireddate=port_date,
                u_custnr=new_contract.u_custnr,
                u_personorcompany=person,
                u_transactiontype=transacttype,
                u_transactionstate=DolTelCeTaState.objects.using("sapserver").get(
                    code=1
                ),
                u_carrier=DolTelCarrier.objects.using("sapserver").get(code=2),
                u_portnum=1,
            )

            onnm_entity2.autokey += 1
            onnm_entity2.save()

            new_transact.save(force_insert=True, using="sapserver")
            new_transact.refresh_from_db()

            transactline1 = DolTelCeTaLines(
                code=new_transact,
                lineid=1,
                object="DOL_TEL_CE_TA",
                u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=1),
                u_transactattrval="TNB-SIP-TRUNK",
            )
            transactline1.save(force_insert=True, using="sapserver")
            transactline2 = DolTelCeTaLines(
                code=new_transact,
                lineid=2,
                object="DOL_TEL_CE_TA",
                u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=2),
                u_transactattrval=m.group(1),
            )
            transactline2.save(force_insert=True, using="sapserver")
            transactline3 = DolTelCeTaLines(
                code=new_transact,
                lineid=3,
                object="DOL_TEL_CE_TA",
                u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=3),
                u_transactattrval=m.group(2),
            )
            transactline3.save(force_insert=True, using="sapserver")
            transactline4 = DolTelCeTaLines(
                code=new_transact,
                lineid=4,
                object="DOL_TEL_CE_TA",
                u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=4),
                u_transactattrval=telnum.u_custnr.zipcode,
            )
            transactline4.save(force_insert=True, using="sapserver")
            transactline5 = DolTelCeTaLines(
                code=new_transact,
                lineid=5,
                object="DOL_TEL_CE_TA",
                u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=5),
                u_transactattrval=telnum.u_custnr.city,
            )
            transactline5.save(force_insert=True, using="sapserver")
            transactline6 = DolTelCeTaLines(
                code=new_transact,
                lineid=6,
                object="DOL_TEL_CE_TA",
                u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=9),
                u_transactattrval=telnum.u_onkz,
            )
            transactline6.save(force_insert=True, using="sapserver")
            transactline7 = DolTelCeTaLines(
                code=new_transact,
                lineid=7,
                object="DOL_TEL_CE_TA",
                u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=10),
                u_transactattrval=quantity,
            )
            transactline7.save(force_insert=True, using="sapserver")
            transactline8 = DolTelCeTaLines(
                code=new_transact,
                lineid=8,
                object="DOL_TEL_CE_TA",
                u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=11),
                u_transactattrval="A187",
            )
            transactline8.save(force_insert=True, using="sapserver")
            transactline9 = DolTelCeTaLines(
                code=new_transact,
                lineid=9,
                object="DOL_TEL_CE_TA",
                u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=21),
                u_transactattrval=telnum.u_wbciidin,
            )
            transactline9.save(force_insert=True, using="sapserver")
            transactline10 = DolTelCeTaLines(
                code=new_transact,
                lineid=10,
                object="DOL_TEL_CE_TA",
                u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=22),
                u_transactattrval=telnum.u_ekpin,
            )
            transactline10.save(force_insert=True, using="sapserver")
            transactline11 = DolTelCeTaLines(
                code=new_transact,
                lineid=11,
                object="DOL_TEL_CE_TA",
                u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=23),
                u_transactattrval=0,
            )
            transactline11.save(force_insert=True, using="sapserver")

            if new_transact.u_personorcompany == 0:
                transactline12 = DolTelCeTaLines(
                    code=new_transact,
                    lineid=12,
                    object="DOL_TEL_CE_TA",
                    u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(
                        code=12
                    ),
                    u_transactattrval=telnum.u_custnr.cardname,
                )
                transactline12.save(force_insert=True, using="sapserver")

                if transacttype.code == 2:
                    transactline13 = DolTelCeTaLines(
                        code=new_transact,
                        lineid=13,
                        object="DOL_TEL_CE_TA",
                        u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(
                            code=24
                        ),
                        u_transactattrval=telnum.u_mainnum,
                    )
                    transactline13.save(force_insert=True, using="sapserver")
                    transactline14 = DolTelCeTaLines(
                        code=new_transact,
                        lineid=14,
                        object="DOL_TEL_CE_TA",
                        u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(
                            code=25
                        ),
                        u_transactattrval=0,
                    )
                    transactline14.save(force_insert=True, using="sapserver")
                    transactline15 = DolTelCeTaLines(
                        code=new_transact,
                        lineid=15,
                        object="DOL_TEL_CE_TA",
                        u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(
                            code=26
                        ),
                        u_transactattrval=str(telnum.u_blockfrom).zfill(
                            len(str(telnum.u_blockto))
                        ),
                    )
                    transactline15.save(force_insert=True, using="sapserver")
                    transactline16 = DolTelCeTaLines(
                        code=new_transact,
                        lineid=16,
                        object="DOL_TEL_CE_TA",
                        u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(
                            code=27
                        ),
                        u_transactattrval=telnum.u_blockto,
                    )
                    transactline16.save(force_insert=True, using="sapserver")
                else:
                    transactline13 = DolTelCeTaLines(
                        code=new_transact,
                        lineid=13,
                        object="DOL_TEL_CE_TA",
                        u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(
                            code=14
                        ),
                        u_transactattrval=telnum.u_mainnum,
                    )
                    transactline13.save(force_insert=True, using="sapserver")
            else:
                transactline12 = DolTelCeTaLines(
                    code=new_transact,
                    lineid=12,
                    object="DOL_TEL_CE_TA",
                    u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(
                        code=7
                    ),
                    u_transactattrval="VORNAME",
                )
                transactline12.save(force_insert=True, using="sapserver")
                transactline13 = DolTelCeTaLines(
                    code=new_transact,
                    lineid=13,
                    object="DOL_TEL_CE_TA",
                    u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(
                        code=8
                    ),
                    u_transactattrval="NACHNAME",
                )
                transactline13.save(force_insert=True, using="sapserver")
                transactline14 = DolTelCeTaLines(
                    code=new_transact,
                    lineid=14,
                    object="DOL_TEL_CE_TA",
                    u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(
                        code=6
                    ),
                    u_transactattrval="ANREDE",
                )
                transactline14.save(force_insert=True, using="sapserver")
                transactline15 = DolTelCeTaLines(
                    code=new_transact,
                    lineid=15,
                    object="DOL_TEL_CE_TA",
                    u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(
                        code=14
                    ),
                    u_transactattrval=int(telnum.u_mainnum),
                )
                transactline15.save(force_insert=True, using="sapserver")

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
