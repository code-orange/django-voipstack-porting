import io
from datetime import date

from PyPDF2 import PdfFileWriter, PdfFileReader
from django.core.management.base import BaseCommand
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.pdfgen import canvas


class Command(BaseCommand):
    help = "Generate Port doc"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        datenow = date.today()

        port_doc_from = "Example Telco"
        port_doc_from_short = "Telco"
        port_doc_from_email = "porting@example.com"
        port_doc_from_tel = 219285490
        port_doc_to = "Other Telco Deutschland GmbH"
        port_doc_cancel = 1
        port_doc_porting = 1
        port_doc_all_numbers = 1
        port_doc_migrate_ressources = 0
        port_doc_safe_harbour = 1
        port_doc_port_in = 1

        port_doc_reason = ""
        port_custom_text = ""

        port_doc_name_or_company = "Mustermann"
        port_doc_firstname = "Max"
        port_doc_street = "Musterstr."
        port_doc_hnum = "46"
        port_doc_zipcode = "12345"
        port_doc_city = "Musterstadt"

        port_doc_wbci_gf = "VA-KUE-MRN"
        port_doc_wbci_vid = "DEU.FOO.V000001055"
        port_doc_wbci_sid = "DEU.FOO.S000001055"
        port_doc_change_date = date.today()
        port_doc_change_date_new = date.today()

        port_doc_port_time = 1

        port_doc_onkz = 2192
        port_doc_num_single1 = 1234
        port_doc_pki_single1 = "D027"
        port_doc_num_single2 = 1111
        port_doc_pki_single2 = "D027"
        port_doc_num_single3 = 2222
        port_doc_pki_single3 = "D027"
        port_doc_num_single4 = 3333
        port_doc_pki_single4 = "D027"
        port_doc_num_single5 = 444444
        port_doc_pki_single5 = "D027"
        port_doc_num_single6 = 55555555
        port_doc_pki_single6 = "D027"
        port_doc_num_single7 = 666
        port_doc_pki_single7 = "D027"
        port_doc_num_single8 = 77777
        port_doc_pki_single8 = "D027"
        port_doc_num_single9 = 888888
        port_doc_pki_single9 = "D027"
        port_doc_num_single10 = 999
        port_doc_pki_single10 = "D027"

        port_doc_num_block_base = 8549
        port_doc_num_block_main = 0
        port_doc_num_block_start = 0
        port_doc_num_block_end = 29
        port_doc_num_block_pki = "D027"

        packet = io.BytesIO()
        # create a new PDF with Reportlab
        can = canvas.Canvas(packet, pagesize=A4)
        can.setFont("Helvetica", 18)

        if port_doc_cancel:
            can.drawString(1.40 * cm, 27.20 * cm, "X")

        if port_doc_porting:
            can.drawString(1.40 * cm, 25.45 * cm, "X")

        if port_doc_all_numbers:
            can.drawString(1.40 * cm, 22.45 * cm, "X")

        can.setFont("Helvetica", 11)

        can.drawString(7.90 * cm, 15.60 * cm, str(port_doc_wbci_vid))
        can.drawString(14.66 * cm, 15.60 * cm, str(port_doc_wbci_sid))

        can.setFont("Helvetica", 12)

        can.drawString(7.90 * cm, 28.30 * cm, str(port_doc_from))
        can.drawString(13.10 * cm, 26.8 * cm, str(port_doc_to))

        can.drawString(4.00 * cm, 24.80 * cm, str(port_doc_name_or_company))
        can.drawString(14.50 * cm, 24.80 * cm, str(port_doc_firstname))
        can.drawString(3.4 * cm, 24.25 * cm, str(port_doc_street))
        can.drawString(14.35 * cm, 24.25 * cm, str(port_doc_hnum))
        can.drawString(2.92 * cm, 23.70 * cm, str(port_doc_zipcode))
        can.drawString(7.82 * cm, 23.70 * cm, str(port_doc_city))

        can.drawString(5.40 * cm, 22.45 * cm, str(port_doc_onkz))

        can.drawString(9.53 * cm, 22.45 * cm, str(port_doc_num_single1))
        can.drawString(13.03 * cm, 22.45 * cm, str(port_doc_num_single2))
        can.drawString(16.79 * cm, 22.45 * cm, str(port_doc_num_single3))

        can.drawString(9.53 * cm, 21.90 * cm, str(port_doc_num_single4))
        can.drawString(13.03 * cm, 21.90 * cm, str(port_doc_num_single5))
        can.drawString(16.79 * cm, 21.90 * cm, str(port_doc_num_single6))

        can.drawString(9.53 * cm, 21.36 * cm, str(port_doc_num_single7))
        can.drawString(13.03 * cm, 21.36 * cm, str(port_doc_num_single8))
        can.drawString(16.79 * cm, 21.36 * cm, str(port_doc_num_single9))
        can.drawString(5.40 * cm, 21.36 * cm, str(port_doc_num_single10))

        can.drawString(5.40 * cm, 20.24 * cm, str(port_doc_num_block_base))
        can.drawString(10.6 * cm, 20.24 * cm, str(port_doc_num_block_main))
        can.drawString(
            13.72 * cm,
            20.24 * cm,
            str(port_doc_num_block_start).zfill(len(str(port_doc_num_block_end))),
        )
        can.drawString(17.3 * cm, 20.24 * cm, str(port_doc_num_block_end))
        can.drawString(
            3.40 * cm,
            17.07 * cm,
            str(port_doc_city) + ", " + datenow.strftime("%d.%m.%Y"),
        )

        can.drawString(3.23 * cm, 15.60 * cm, str(port_doc_wbci_gf))

        can.drawString(8.86 * cm, 14.78 * cm, port_doc_change_date.strftime("%d.%m.%Y"))
        can.drawString(
            16.18 * cm, 14.78 * cm, port_doc_change_date_new.strftime("%d.%m.%Y")
        )

        # Portierungsfenster
        if port_doc_port_time == 1:
            # 06:00 - 8:00
            can.drawString(5.10 * cm, 14.20 * cm, "X")
        elif port_doc_port_time == 2:
            # 06:00 - 12:00
            can.drawString(9.00 * cm, 14.20 * cm, "X")
        else:
            # custom
            can.drawString(13.60 * cm, 14.20 * cm, "X")
            can.drawString(14.43 * cm, 14.12 * cm, str(port_doc_num_block_end))

        can.drawString(4.52 * cm, 13.56 * cm, str(port_doc_from_short))
        can.setFont("Helvetica", 10)
        can.drawString(10.8 * cm, 13.56 * cm, str(port_doc_from_email))
        can.setFont("Helvetica", 12)
        can.drawString(15.67 * cm, 13.56 * cm, "0" + str(port_doc_from_tel))

        if port_doc_migrate_ressources == 1:
            can.drawString(5.56 * cm, 12.8 * cm, "X")
        else:
            can.drawString(7.04 * cm, 12.8 * cm, "X")

        if port_doc_safe_harbour:
            can.drawString(11.89 * cm, 12.8 * cm, "X")

        if port_doc_cancel == 0:
            can.drawString(18.10 * cm, 12.8 * cm, "X")
        elif port_doc_cancel == 2:
            can.drawString(16.56 * cm, 12.8 * cm, "X")

        # APPROVAL
        can.drawString(4.80 * cm, 12.00 * cm, "X")
        can.drawString(6.65 * cm, 12.00 * cm, "X")
        can.drawString(8.53 * cm, 12.00 * cm, "X")

        can.drawString(
            11.23 * cm, 12.00 * cm, port_doc_change_date.strftime("%d.%m.%Y")
        )
        can.drawString(17.22 * cm, 12.00 * cm, "TECH01")

        can.drawString(2.60 * cm, 11.20 * cm, "X")
        can.drawString(4.80 * cm, 11.20 * cm, "X")
        can.drawString(10.72 * cm, 11.20 * cm, str(port_doc_num_block_end))

        # REASON
        can.drawString(2.49 * cm, 10.31 * cm, str(port_doc_reason))

        # DECLINED
        can.drawString(5 * cm, 9.5 * cm, "X")
        can.drawString(6.8 * cm, 9.5 * cm, "X")
        can.drawString(8.60 * cm, 9.5 * cm, "X")
        can.drawString(10.40 * cm, 9.5 * cm, "X")
        can.drawString(12.10 * cm, 9.5 * cm, "X")
        can.drawString(13.70 * cm, 9.5 * cm, "X")
        can.drawString(15.45 * cm, 9.5 * cm, "X")

        can.drawString(4.88 * cm, 8.69 * cm, str(port_doc_onkz))

        can.drawString(1.30 * cm, 7.59 * cm, str(port_doc_num_single1))
        can.drawString(4.06 * cm, 7.59 * cm, str(port_doc_pki_single1))
        can.drawString(1.30 * cm, 7.04 * cm, str(port_doc_num_single2))
        can.drawString(4.06 * cm, 7.04 * cm, str(port_doc_pki_single2))
        can.drawString(1.30 * cm, 6.50 * cm, str(port_doc_num_single3))
        can.drawString(4.06 * cm, 6.50 * cm, str(port_doc_pki_single3))
        can.drawString(1.30 * cm, 5.94 * cm, str(port_doc_num_single4))
        can.drawString(4.06 * cm, 5.94 * cm, str(port_doc_pki_single4))
        can.drawString(1.30 * cm, 5.38 * cm, str(port_doc_num_single5))
        can.drawString(4.06 * cm, 5.38 * cm, str(port_doc_pki_single5))

        can.drawString(6.68 * cm, 7.59 * cm, str(port_doc_num_single6))
        can.drawString(9.50 * cm, 7.59 * cm, str(port_doc_pki_single6))
        can.drawString(6.68 * cm, 7.04 * cm, str(port_doc_num_single7))
        can.drawString(9.50 * cm, 7.04 * cm, str(port_doc_pki_single7))
        can.drawString(6.68 * cm, 6.50 * cm, str(port_doc_num_single8))
        can.drawString(9.50 * cm, 6.50 * cm, str(port_doc_pki_single8))
        can.drawString(6.68 * cm, 5.94 * cm, str(port_doc_num_single9))
        can.drawString(9.50 * cm, 5.94 * cm, str(port_doc_pki_single9))
        can.drawString(6.68 * cm, 5.38 * cm, str(port_doc_num_single10))
        can.drawString(9.50 * cm, 5.38 * cm, str(port_doc_pki_single10))

        can.drawString(13.03 * cm, 7.04 * cm, str(port_doc_num_block_base))
        can.drawString(16.76 * cm, 7.04 * cm, str(port_doc_num_block_main))

        can.drawString(
            14.38 * cm,
            5.94 * cm,
            str(port_doc_num_block_start).zfill(len(str(port_doc_num_block_end))),
        )
        can.drawString(16.79 * cm, 5.94 * cm, str(port_doc_num_block_end))

        can.drawString(14.40 * cm, 5.38 * cm, str(port_doc_num_block_pki))

        can.drawString(4.06 * cm, 4.60 * cm, str(port_doc_from_short))
        can.drawString(10.06 * cm, 4.60 * cm, str(port_doc_from_email))
        can.drawString(15.77 * cm, 4.60 * cm, "0" + str(port_doc_from_tel))

        can.setFont("Helvetica", 16)
        can.drawString(1.32 * cm, 3.50 * cm, str(port_custom_text))

        can.save()

        # move to the beginning of the StringIO buffer
        packet.seek(0)
        new_pdf = PdfFileReader(packet)
        # read your existing PDF
        existing_pdf = PdfFileReader(
            open(
                "django_voipstack_porting/templates/django_voipstack_porting/portierung.pdf",
                "rb",
            )
        )
        output = PdfFileWriter()
        # add the "watermark" (which is the new pdf) on the existing page
        page = existing_pdf.getPage(0)
        page.mergePage(new_pdf.getPage(0))
        output.addPage(page)
        # finally, write "output" to a real file
        outputStream = open("tmp/destination.pdf", "wb")
        output.write(outputStream)
        outputStream.close()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
