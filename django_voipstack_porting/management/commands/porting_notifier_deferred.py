import slumber
from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.models import F
from django.template import engines

from dit_enterprisehub_sap.models import *


class Command(BaseCommand):
    help = "Sends email notifications for deferred porting requests"

    def handle(self, *args, **options):
        api = slumber.API(
            settings.ZAMMAD_URL, auth=(settings.ZAMMAD_USER, settings.ZAMMAD_PASSWD)
        )

        txt_ticketmail_header = DolMainTemplates.objects.using("sapserver").get(
            name="email_template_header"
        )
        txt_ticketmail_text = DolMainTemplates.objects.using("sapserver").get(
            name="email_template_terminverschiebung_der_portierung"
        )
        txt_ticketmail_footer = DolMainTemplates.objects.using("sapserver").get(
            name="email_template_footer"
        )
        txt_ticketmail = "\r\n".join(
            [
                txt_ticketmail_header.u_template,
                txt_ticketmail_text.u_template,
                txt_ticketmail_footer.u_template,
            ]
        )

        django_engine = engines["django"]

        # New Port Requests
        self.stdout.write("Deferred Port Requests")

        all_open_port = (
            DolTelPortNotifr.objects.using("sapserver")
            .exclude(u_portjobid__u_portdate=F("u_sentinstant"))
            .exclude(u_portjobid__u_portdate="2999-01-01 00:00:00")
            .exclude(u_portjobid__u_ticketid=None)
            .select_related()
            .order_by("u_portjobid__u_portdate")
        )

        print(all_open_port.query)

        for p in all_open_port:
            print("============================")
            print(p.u_portjobid.u_mainnum)
            print(p.u_portjobid.u_custnr.cardcode)
            print(p.u_portjobid.u_custnr.cardname)
            print(p.u_portjobid.u_custnr.e_mail)
            print("============================")

            template_opts = dict()
            template_opts["customer_code"] = p.u_portjobid.u_custnr.cardcode
            template_opts["customer_name"] = p.u_portjobid.u_custnr.cardname
            template_opts["customer_email"] = p.u_portjobid.u_custnr.e_mail
            template_opts["port_date"] = p.u_portjobid.u_portdate.strftime("%d.%m.%Y")
            template_opts["port_onkz"] = p.u_portjobid.u_onkz
            template_opts["port_mainnum"] = p.u_portjobid.u_mainnum
            template_opts["port_blkstart"] = p.u_portjobid.u_blockfrom
            template_opts["port_blkend"] = p.u_portjobid.u_blockto

            ticket_title = (
                "Portierung 0"
                + str(p.u_portjobid.u_onkz)
                + "/"
                + str(p.u_portjobid.u_mainnum)
            )

            template_ticketmail = django_engine.from_string(txt_ticketmail)
            articledata = dict()
            articledata["ticket_id"] = p.u_portjobid.u_ticketid
            articledata["to"] = p.u_address
            articledata["subject"] = ticket_title
            articledata["body"] = template_ticketmail.render(template_opts)
            articledata["content_type"] = "text/plain"
            articledata["type"] = "email"
            articledata["internal"] = False

            article = api.ticket_articles.post(articledata)

            # set new info
            p.u_sentinstant = p.u_portjobid.u_portdate
            p.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
