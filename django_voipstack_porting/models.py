from django_simple_notifier.django_simple_notifier.models import (
    SnotifierEmailContactZammadAbstract,
)
from django_voipstack_mdat.django_voipstack_mdat.models import *


class VoipPortOrder(models.Model):
    id = models.BigAutoField(primary_key=True)
    wbci_id = models.CharField(unique=True, max_length=20, blank=True, null=True)
    customer = models.ForeignKey(MdatCustomers, models.CASCADE, db_column="customer")
    carrier = models.ForeignKey(VoipMdatCarrier, models.CASCADE, db_column="carrier")
    numbers = models.ManyToManyField("django_voipstack_mdat.VoipMdatNumbers")
    date_added = models.DateTimeField(default=datetime.now)
    date_porting = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = "voip_port_order"


class VoipPortNotifierEmail(SnotifierEmailContactZammadAbstract):
    number = models.ForeignKey(VoipMdatNumbers, models.CASCADE, db_column="number")
    sent_instant = models.DateTimeField(default=datetime.now)
    sent_reminder = models.DateTimeField(default=datetime.now)
    sent_done = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "voip_port_notifier"


class VoipPortAioNotifierEmail(SnotifierEmailContactZammadAbstract):
    port_order = models.ForeignKey(
        VoipPortOrder, models.CASCADE, db_column="port_order"
    )
    sent_instant = models.DateTimeField(default=datetime.now)
    sent_reminder = models.DateTimeField(default=datetime.now)
    sent_done = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "voip_port_aio_notifier"
