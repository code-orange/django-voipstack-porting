# Generated by Django 2.1.5 on 2019-01-09 21:25

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_voipstack_mdat", "0006_add_unique_for_carriers_main"),
        ("django_voipstack_porting", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="voipportorder",
            name="numbers",
            field=models.ManyToManyField(to="django_voipstack_mdat.VoipMdatNumbers"),
        ),
    ]
